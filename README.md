# Page de l'UE MIF24 Bases de Données non relationnelles

[Master Informatique](http://master-info.univ-lyon1.fr/M1/) de l'Université Lyon 1.

## Année 2024-2025

Intervenants: [Emmanuel Coquery](https://perso.liris.cnrs.fr/emmanuel.coquery/) et [Fabien Duchateau](https://perso.liris.cnrs.fr/fabien.duchateau/)

Évaluation: 60% QCMs au début de chaque cours (sauf le premier), 40% ECA

### Séances et supports

| Séance    | Type            | Contenu                             | Supports                                                                                                 |
| --------- | --------------- | ----------------------------------- | -------------------------------------------------------------------------------------------------------- |
| 06/09     | CM 1            | Chemins                             | [Diapos](cm/cm1-chemins.pdf)                                                                             |
|           | TD 1            | Chemins                             | [Sujet](td/td1-chemins.pdf), [Corrigé](td/td1-chemins-corrige.pdf), [Code ex.3](td/td1-chemins.code.zip) |
|           | TP 1            | Chemins (JSON postgres)             | [Sujet](tp/tp1-chemins.md), [Corrigé](tp/tp1-corrige.sql)                                                |
| 13/09     | CM 2            | Patterns de documents               |                                                                                                          |
|           | TP 2 (G1/G2)    | Requêtes Mongo (find / aggregation) | [Sujet](tp/tp2-mongo.md), [Corrigé](tp/tp2-mongo.mongodb)                                                |
| 17/09     | TP 2 (G3)       | Requêtes Mongo (find / aggregation) | [Sujet](tp/tp2-mongo.md), [Corrigé](tp/tp2-mongo.mongodb)                                                |
| --        |                 |                                     |                                                                                                          |
| 04/10     | CM 3            | JSON schema, types algèbre          | [Diapos](cm/cm3-types-json-schema.pdf)                                                                   |
|           | TD 2 (G1/G2 )   | JSON schema, types algèbre          | [Sujet](td/td2-types.pdf)                                                                                |
| 08/10     | TD 2 (G3)       | JSON schema, types algèbre          | [Sujet](td/td2-types.pdf)                                                                                |
| 11/10     | CM 4            | Algèbre                             | [Diapos](cm/cm4-algebres-collections.pdf), [Code](cm/cm4-code.md)                                        |
|           | TD 3 (G1/G2)    | Algèbre                             | [Sujet](td/td3-algebre.pdf), [Corrigé](td/td3-algebre-correction.pdf)                                    |
| 15/10     | TD 3 (G3)       | Algèbre                             | [Sujet](td/td3-algebre.pdf), [Corrigé](td/td3-algebre-correction.pdf)                                    |
| --        |                 |                                     |                                                                                                          |
| ~~01/11~~ |                 |                                     |                                                                                                          |
| 08/11     | CM 5            | Code algèbre / Exemples Mongo       | suite du 11/10, [Diapos](cm/cm4-algebres-collections.pdf), [Code](cm/cm4-code.md)                        |
|           | TP 3 (G1/G2)    | Requêtes Mongo (aggregation)        | [Sujet](tp/tp3-mongo-aggregation.md)                                                                     |
| 13/11     | TP 3 (G3)       | Requêtes Mongo (aggregation)        | [Sujet](tp/tp3-mongo-aggregation.md)                                                                     |
| --        |                 |                                     |                                                                                                          |
| 26/11     | CM 6            | RDF SPARQL                          | [Diapos](cm/cm6-rdf.pdf)                                                                                 |
| 27/11     | TD 4 (G1/G2)    | RDF SPARQL                          | [Sujet](td/td4-rdf-sparql.pdf)                                                                           |
|           | TP 4 (G1/G2)    | RDF SPARQL                          | [Sujet](tp/tp4-sparql.md)                                                                                |
| 03/12     | TD 4 (G3)       | RDF SPARQL                          | [Sujet](td/td4-rdf-sparql.pdf)                                                                           |
| 04/12     | TP 4 (G3)       | RDF SPARQL                          | [Sujet](tp/tp4-sparql.md)                                                                                |
| 06/12     | CM 7            | RDF SHACL                           | [Diapos](cm/cm7-shacl.pdf)                                                                               |
|           | TD 5 (G1/G2)    | RDF SHACL                           | [Sujet](td/td5-shacl.pdf), [Corrigé](td/td5-shacl-correction.pdf)                                        |
| 09/12     | TD 5 (G3)       | RDF SHACL                           | [Sujet](td/td5-shacl.pdf), [Corrigé](td/td5-shacl-correction.pdf)                                        |
| --        |                 |                                     |                                                                                                          |
| 20/12     | CM 8            | ORM                                 | [Diapos](cm/cm8-orm.pdf)                                                                                 |
|           | TP 5 (G1/G2/G3) | ORM                                 | [Dépôt du sujet](https://forge.univ-lyon1.fr/mif24-bdnosql/mif24-bdnosql-orm)                            |
| --        |                 |                                     |                                                                                                          |
| 08/01     | TP 6 (G3)       | ORM                                 |                                                                                                          |
|           | TD 6 (G3)       | Révisions                           | [Sujet](td/td6-revisions.pdf), [Corrigé](td/td6-revisions-correction.pdf)                                |
| 10/01     | TP 6 (G1/G2)    | ORM                                 |                                                                                                          |
|           | TD 6 (G1/G2)    | Révisions                           | [Sujet](td/td6-revisions.pdf), [Corrigé](td/td6-revisions-correction.pdf)                                |
| 20/01 (?) | ECA             |                                     |                                                                                                          |

### Sujets d'examen / ECA

- [sujet 2023](annales/eca-23-24-s1.pdf)
- [sujet 2021](http://perso.liris.cnrs.fr/emmanuel.coquery/home/enseignement/archives/mif04/mif04-examen-2021-2022-s1.pdf), avec [son corrigé](http://perso.liris.cnrs.fr/emmanuel.coquery/home/enseignement/archives/mif04/mif04-examen-2021-2022-s1-corrige) et des [explications en vidéo](https://scalelite-info.univ-lyon1.fr/playback/presentation/2.3/514be0833f3339fc1b5110968f76d29369c161c5-1644316265552)
- [sujet 2020](http://perso.liris.cnrs.fr/emmanuel.coquery/home/enseignement/archives/mif04/mif04-examen-2020-2021-s1.pdf)
- [sujet 2019](http://perso.liris.cnrs.fr/emmanuel.coquery/home/enseignement/archives/mif04/mif04-examen-2019-2020-s1.pdf)
- [sujet 2018](http://perso.liris.cnrs.fr/emmanuel.coquery/home/enseignement/archives/mif04/mif04-examen-2018-2019-s1.pdf)
